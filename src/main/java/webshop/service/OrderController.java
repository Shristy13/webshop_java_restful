package webshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import webshop.assembler.OrderResourceAssembler;
import webshop.model.Order;
import webshop.processor.OrderRepository;
import webshop.resources.OrderResource;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins ="*")
@RestController
@ExposesResourceFor(Order.class)
@RequestMapping(value = "/order" , produces =" application/json")
public class OrderController {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderResourceAssembler assembler;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<OrderResource>> findAllOrders() {
        List<Order> orders = repository.findAll();
        return new ResponseEntity<>(assembler.toResourceCollection(orders), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}" , method = RequestMethod.GET)
    public ResponseEntity<OrderResource> findOrderById(@PathVariable Long id) {
        Optional<Order>  order = repository.findById(id);

        // Check if the order is present :
        if(order.isPresent()) {
            return  new ResponseEntity<>(assembler.toResource(order.get()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<OrderResource> createOrder(@RequestBody Order order) {

        Order createOrder = repository.create(order);
        return new ResponseEntity<>(assembler.toResource(createOrder),HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}" , method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteOrder(@PathVariable Long id ) {
        // Delete the order by id :
        boolean wasDeleted = repository.delete(id);
        HttpStatus responseStatus = wasDeleted? HttpStatus.NO_CONTENT:HttpStatus.NOT_FOUND;
        return  new ResponseEntity<>(responseStatus);
    }

    /**
     * Function to update the order :
     */
    public ResponseEntity<OrderResource> updateOrder(@PathVariable Long id , @RequestBody Order updateOrder) {

        boolean wasUpdated = repository.update(id,updateOrder);

        // Check if the order was
        if(wasUpdated) {
            return findOrderById(id);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
