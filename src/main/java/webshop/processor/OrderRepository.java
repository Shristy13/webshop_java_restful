package webshop.processor;

import org.springframework.stereotype.Repository;
import webshop.model.Identifiable;
import webshop.model.Order;

@Repository
public class OrderRepository extends InMemoryRepository<Order> {

    @Override
    protected void updateIfExists(Order original, Order updated) {
        original.setDescription(updated.getDescription());
        original.setComplete(updated.isComplete());
        original.setCostInCents(updated.getCostInCents());
    }
}
