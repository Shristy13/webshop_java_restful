package webshop.resources;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import webshop.model.Order;


public class OrderResource extends ResourceSupport {

    private final long id;
    private final String description;
    private final long costInCents;
    private final boolean isComplete;

    public OrderResource(Order order) {
        this.id = order.getId();
        this.description = order.getDescription();
        this.costInCents = order.getCostInCents();
        this.isComplete = order.isComplete();
    }

    @JsonProperty
    public Long getResourceId(){
        return  id;
    }

    public long getCostInCents() {
        return costInCents;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public String getDescription(){
        return description;
    }
}
