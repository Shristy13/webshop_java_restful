package webshop.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import webshop.model.Order;
import webshop.resources.OrderResource;

@Component
public class OrderResourceAssembler extends ResourceAssembler<Order, OrderResource> {

    @Autowired
    protected EntityLinks entityLinks;

    private static final String UPDATE_REL = "update";
    private static final String DELETE_REL = "delete";

    @Override
    public OrderResource toResource(Order orderObject) {
        // Create the resource object :
        OrderResource resource = new OrderResource(orderObject);

        final Link selfLink =entityLinks.linkToSingleResource(orderObject);

        resource.add(selfLink.withSelfRel());
        resource.add(selfLink.withRel(UPDATE_REL));
        resource.add(selfLink.withRel(DELETE_REL));

        return resource;
    }
}
